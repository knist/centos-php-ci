FROM centos:7

MAINTAINER Jarl Walker <jwalker@walkerallen.com>

RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
RUN yum update -y
RUN yum clean all
RUN yum install yum-utils -y
RUN yum install epel-release
RUN yum-config-manager --enable remi-php73
RUN yum install -y supervisor wget vim git nginx openssl mod_ssl php php-pecl-stomp php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-intl php-soap php-common php-pdo php-curl php-xml php-pgsql php-mbstring php-pecl-memcached php-fpm

# Installing pip and aws-cli
RUN yum install -y python-setuptools python-pip
RUN pip install awscli

# Installing composer
RUN wget -O /usr/local/bin/composer https://getcomposer.org/download/1.9.3/composer.phar
RUN chmod +x /usr/local/bin/composer


# Installing phpunit
RUN composer global require "phpunit/phpunit=^5.0"
RUN composer global require phpmd/phpmd
RUN composer global require squizlabs/php_codesniffer
#RUN composer global require sebastian/phpcpd
ENV PATH="/root/.composer/vendor/bin:${PATH}"

RUN wget -O /usr/local/bin/codecept http://codeception.com/codecept.phar
RUN chmod a+x /usr/local/bin/codecept

